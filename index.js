// index.js

const fs = require('fs');
const pm2 = require('pm2');

const files = fs.readdirSync('data/chunks');



if (!files.length) {
  console.error('You need to split the csv file with npm run split-csv command in CLI.')
  process.exit()
}

function startProcess(fileIndex) {
  const chunk = 'chunk-' + fileIndex + '.csv';

  pm2.start({
    name: chunk,
    script: 'utils/register_mongodb.js',
    exec_mode: 'cluster',
    instances:2,
    args: chunk,
    max_memory_restart: '4G',
    autorestart: false,
    watch: false,
    out_file: './logs/chunks/chunk.log',
  }, function (err, apps) {
    if (err) {
      console.error('Error in PM2:', err, apps)
      pm2.disconnect();
    }
  });
}

pm2.connect(function (err) {
  if (err) {
    console.error('Error in PM2 connect', err);
    process.exit(2);
  }

  let fileToRegister = 0;
  let timer = 0;
  let paused = false;

  for (let processes = 0; processes < 6; processes++) {
    startProcess(fileToRegister)
    fileToRegister++
  }

  pm2.launchBus(function (err, bus) {
    bus.on('process:end', function (packet) {
      pm2.delete(packet.process.name, function (e) {
        if (e) {
          console.error('Error on process deleting.', e)
          process.exit()
        }
      });
      timer = timer + packet.data.timer
      if (fileToRegister >= files.length) {
        pm2.list(function(err, list) {
          timer = Number((timer).toFixed(0))

          console.log('Remaining processes: ' + list.length + ', total execution time in s: ' + timer
            + ', or ' + Math.trunc(timer / 60) + ' minutes and ' + timer % 60 + 'seconds');
        })
      } else {
        startProcess(fileToRegister)
        fileToRegister++
      }
    });
    bus.on('process:error', function (packet) {
      console.error('Error during script execution: ', packet.data.error)
      pm2.disconnect();
    });

    bus.on('pause', function () {
      console.log('Pause event received');
      paused = true;
      pm2.list((err, list) => {
        list.map(item => {
          pm2.pause(item.pm_id, function (err, proc) {
            if (err) {
              console.error('Error pausing process:', err);
              process.exit();
            }
            console.log('Process', proc[0].name, 'paused');
          });
        });
      });
    });

    bus.on('resume', function () {
      console.log('Resume event received');
      paused = false;
      pm2.list((err, list) => {
        list.map(item => {
          pm2.resume(item.pm_id, function (err, proc) {
            if (err) {
              console.error('Error resuming process:', err);
              process.exit();
            }
            console.log('Process', proc[0].name, 'resumed');
          });
        });
      });
    });

    bus.on('check_pause', function () {
      if (paused) {
        console.log('Processes are currently paused');
      } else {
        console.log('Processes are not paused');
      }
    });
  });
});
