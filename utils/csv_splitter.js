// csv_splitter.js

const csvSplitStream = require('csv-split-stream');
const fs = require('fs');

const chunkDirectory = './data/chunks';

async function splitcsv() {
  try {
    console.log('Inside');
    fs.mkdirSync(chunkDirectory, { recursive: true });
    await csvSplitStream.split(
      fs.createReadStream('./data/Stock.csv'),
      {
        lineLimit: 50000,
      },
      (index) => {
        return fs.createWriteStream(`./data/chunks/chunk-${index}.csv`);
      }
    );
    console.log('CSV splitting succeeded.');
  } catch (error) {
    console.log('CSV splitting failed:', error);
    process.exit();
  }
}

if (fs.existsSync(chunkDirectory)) {
  console.log('There already is a chunks directory. Please remove it before new splitting.');
  process.exit();
}

splitcsv();
