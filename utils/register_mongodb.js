const fs = require('fs');
const csv = require('csvtojson');
const { Enterprise, onConnection } = require('./mongoose'); // Importer l'objet Enterprise et la fonction onConnection depuis le fichier mongodb.js

// Fonction pour diviser un tableau en chunks
function splitArray(array, chunkSize) {
  const splittedArray = [];
  for (let i = 0; i < array.length; i += chunkSize) {
    splittedArray.push(array.slice(i, i + chunkSize));
  }
  return splittedArray;
}

try {
  onConnection(async () => {
    const chunk = process.argv[2];
    console.log('Chunk:', chunk);

    // Définir une fonction hrtime si elle n'est pas déjà définie
    function hrtime(startTime, unit) {
      const NS_PER_SEC = 1e9;
      const NS_PER_MS = 1e6;
      const NS_PER_S = 1e3;
      const NS_PER_MIN = 6e7;

      const diff = process.hrtime(startTime);
      switch (unit) {
        case 's':
          return diff[0] + diff[1] / NS_PER_SEC;
        case 'ms':
          return diff[0] * NS_PER_S + diff[1] / NS_PER_MS;
        case 'min':
          return diff[0] / NS_PER_MIN;
        default:
          return diff[0] * NS_PER_S + diff[1] / NS_PER_MS;
      }
    }

    const executionTime = process.hrtime(); // Utiliser process.hrtime() pour obtenir le temps de début d'exécution
    
    console.time('Translate in JSON');
    const jsonArray = await csv().fromFile(`data/chunks/${chunk}`);
    console.log('test:', jsonArray);
    console.timeEnd('Translate in JSON');

    const splittedJsonArray = splitArray(jsonArray, 1000);

    console.time('Bulk insertion');
    for (let i = 0; i < splittedJsonArray.length; i++) {
      console.log(`Iteration ${i + 1}/${splittedJsonArray.length}`);
      await Enterprise.insertMany(splittedJsonArray[i]);
    }
    console.timeEnd('Bulk insertion');

    process.send({
      type: 'process:end',
      data: {
        timer: hrtime(executionTime, 's') // Utiliser la fonction hrtime pour calculer le temps écoulé
      }
    });
  });
} catch (error) {
  console.error('Error:', error);
  process.send({
    type: 'process:error',
    data: {
      error: error.message
    }
  });
}
