const mongoose = require('mongoose');

const enterpriseSchema = new mongoose.Schema({
  siren: { type: String, unique: true, required: true },
  nic: String,
  siret: { type: String, unique: true, required: true },
  statutDiffusionEtablissement: String,
  dateCreationEtablissement: String,
  // Ajoutez d'autres champs de schéma au besoin
});

const Enterprise = mongoose.model('Enterprise', enterpriseSchema);

function onConnection(callback) {
  if (typeof callback !== 'function') {
    throw new Error('You must send a callback as the first argument.');
  }

  mongoose.connect("mongodb://localhost:27017/indexer", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  mongoose.connection.on('error', (e) => {
    console.error('Mongoose error during process execution:', e);
    throw e;
  });

  mongoose.connection.once('open', function () {
    console.log('Connected to MongoDB');
    callback();
  });
}

module.exports = {
  Enterprise,
  onConnection
};
